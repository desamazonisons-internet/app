import React from "react";
import IsUsingAmazon from "../components/is_using_amazon";
import Header from "../components/header";

export default function HomePage() {
    return (
        <section className="hero is-light is-fullheight is-bold">
            <div className="hero-head">
                <Header isHome={true}/>
            </div>

            <div className="hero-body">
                <div className="container has-text-centered">
                    <h1 className="title">
                        Pour un internet sans Amazon
                    </h1>
                    <h2 className="subtitle" style={{'marginTop': '10vh'}}>
                        Scanner un site pour savoir s'il est hébergé chez Amazon Web Services
                    </h2>
                    <div className="columns" style={{'marginBottom': '5vh'}}>
                        <div className="column is-half is-offset-one-quarter">
                            <IsUsingAmazon/>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    );
}
