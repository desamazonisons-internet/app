import React, {FunctionComponent} from "react";
import Layout from "../components/layout";

interface ArticleProps {
    title: string;
    link: string;
    author: string
}

const Article: FunctionComponent<ArticleProps> = ({title, link, author}) => {
    return (
        <li>
            <a href={link}> {title} </a>
            <div><small>par {author}</small></div>
        </li>
    )
};

export default function WhyPage() {
    return (
        <Layout>
            <section className="container" style={{maxWidth: '800px'}}>
                <h1 className={"title"} style={{marginTop: '10vh'}}>Pourquoi un internet sans Amazon ?</h1>
                <div className="columns" style={{marginTop: '5vh'}}>
                    <div className="column content">

                        <ul>
                            <Article title="Rapport sur l’« impunité fiscale, sociale et environnementale d’Amazon »"
                                     link="https://france.attac.org/nos-publications/notes-et-rapports/article/nouveau-rapport-impunite-fiscale-sociale-et-environnementale-immersion-dans-le"
                                     author="Attac, Les Amis de la Terre, et le syndicat Solidaires">
                            </Article>
                            <Article
                                title="Le cauchemar de la livraison à domicile : « La Mairie de Paris veut maîtriser et taxer les livraisons d’Amazon »"
                                link="https://www.lemonde.fr/economie/article/2019/11/25/anne-hidalgo-veut-maitriser-et-taxer-les-livraisons-d-amazon-a-paris_6020446_3234.html"
                                author="Alexandre Piquard - Le Monde">
                            </Article>
                            <Article
                                title="Amazon : « Derrière le héros du néolibéralisme 2.0 se cache une vision du monde que nous devons combattre »"
                                link="https://www.lemonde.fr/idees/article/2019/11/15/amazon-derriere-le-heros-du-neoliberalisme-2-0-se-cache-une-vision-du-monde-que-nous-devons-combattre_6019227_3232.html"
                                author="un collectif d’associations, d’ONG, d’intellectuels, parmi lesquels Ken Loach, Christophe Alévêque et Alain Damasio">
                            </Article>
                            <Article title="« Amazon made an $11.2bn profit in 2018 but paid no federal tax »"
                                     link="https://www.theguardian.com/technology/2019/feb/15/amazon-tax-bill-2018-no-taxes-despite-billions-profit"
                                     author="The Guardian">
                            </Article>
                        </ul>
                    </div>
                </div>
            </section>
        </Layout>
    );
}
