import * as fs from 'fs';
import * as dns from 'dns';

import fetch from 'isomorphic-unfetch';

import * as ipaddr from 'ipaddr.js';
import {NextApiRequest, NextApiResponse} from "next";

const awsRawData = fs.readFileSync('aws-ip-ranges.json', 'utf8');
const ipv4Ranges: {
    "ip_prefix": string;
    "region": string;
    "service": string;
}[] = JSON.parse(awsRawData)['prefixes'];

function lookupUrl(url: string): Promise<string> {
    return new Promise((resolve, reject) => {
        dns.lookup(url, (err, address) => {
            if (err) reject(err);
            resolve(address);
        });
    });
}

function ipMatchesRange(ip: string, range: string): boolean {
    const parsed_ip = ipaddr.parse(ip);
    const parsed_range = ipaddr.parseCIDR(range);
    // @ts-ignore
    return parsed_ip.match(parsed_range);
}

function lookupAwsInfo(ip: string) {
    return ipv4Ranges.find((prefix) => ipMatchesRange(ip, prefix['ip_prefix']));
}

function cleanUrl(url: string): string {
    return url.replace('http://', '').replace('https://', '').split('/')[0];
}

function tryUrl(url: string) {
    return fetch(`http://${url}`)
        .then(response => {
            console.log(`start_url="${url}" -> final_url="${response.url}"`);
            return {
                urlAfterRedirections: cleanUrl(response.url),
                isProxied: response.headers.get('server') === 'cloudflare'
            };
        });
}

export default async (req: NextApiRequest, res: NextApiResponse) => {
    console.log('query:', req.query);
    const url = req.query.url;

    if (!url) {
        res.status(400).json({'msg': 'missing url'});
        return;
    }
    if (url instanceof Array) {
        res.status(400).json({'msg': 'require single url'});
        return;
    }
    const startUrl = cleanUrl(url);

    let [tryResults, tryError] = await handleError(tryUrl(startUrl));
    if (tryError) {
        console.error(tryError);
        console.error(`Failed to test url=${url}`);
        // Happens in the case of SSL certificate check error (such as "unable to verify the first certificate")
        tryResults = {
            urlAfterRedirections: startUrl,
            isProxied: false,
        };
    }

    const [ip, ipErr] = await handleError(lookupUrl(tryResults.urlAfterRedirections));
    if (ipErr) {
        console.error(ipErr);
        res.status(400).json({'msg': `Failed to find ip for url=${url}`});
        return;
    }

    console.log(`ip=${ip} for url=${url}; ${tryResults.urlAfterRedirections}`);

    const awsInfo = lookupAwsInfo("" + ip);
    console.log(`AWS info for url=${url} -- ${tryResults.urlAfterRedirections}: ${JSON.stringify(awsInfo)}`);

    if (awsInfo) {
        res.json({aws_region: awsInfo['region'], aws_service: awsInfo['service']});
    } else {
        res.json({aws_region: null, aws_service: null, isProxied: tryResults.isProxied});
    }
}


function handleError<T>(promise: Promise<T>) {
    return promise
        .then(data => ([data, null]))
        .catch(error => Promise.resolve([null, error]));
}
