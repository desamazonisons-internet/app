"Des-amazon-isons internet !" project is about:

- easiliy checking if a website is hosted on Amazon Web Services
- documenting why using Amazon Web Services (AWS) and thus Amazon might not be
a good idea. 


# Usage

## Development

- To install the dependencies:

        yarn # or npm install if you prefer
    
- To start the development server:
    
        yarn dev


## Production


- Build the app (with some pages statically prerendered)

        yarn build
        
- Start Nextjs server:

        yarn start


# Contributing

Contributions are most welcome!

You can contribute either to the site or to the bibliography about Amazon 
(contradictory articles are welcome!).
