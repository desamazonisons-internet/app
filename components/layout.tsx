import Header from './header';
import React, {FunctionComponent} from "react";

interface Props {
}

const Layout: FunctionComponent<Props> = props => (
    <div className={''}>
        <Header/>
        <div className="columns">
            <div className={'column'}>
                {props.children}
            </div>
        </div>
    </div>
);

export default Layout;
