import Link from 'next/link';
import React, {FunctionComponent, useState} from "react";

interface Props {
    isHome?: boolean;
}

const Header: FunctionComponent<Props> = ({isHome}) => {
    const [burgerMenuOpen, setBurgerMenuOpen] = useState<boolean>(false);

    const navbarMenuClasses = burgerMenuOpen ? "navbar-menu is-active" : "navbar-menu";
    return (
        <nav className="navbar is-spaced" role="navigation" aria-label="main navigation"
             style={{borderBottom: isHome ? 'none' : '1px solid #eaecef'}}
        >
            <div className="navbar-brand">
                <Link href={'/'}>
                    <a className="navbar-item">
                        Dés-amazon-isons internet !
                    </a>
                </Link>

                <a role="button" className="navbar-burger burger" aria-label="menu" aria-expanded="false"
                   onClick={_ => setBurgerMenuOpen(!burgerMenuOpen)}>
                    <span aria-hidden="true"/>
                    <span aria-hidden="true"/>
                    <span aria-hidden="true"/>
                </a>
            </div>

            <div className={navbarMenuClasses}>
                <div className="navbar-end">
                    <Link href={'/why'}>
                        <a className="navbar-item">
                            Mais pourquoi ?
                        </a>
                    </Link>

                    <a href={'https://gitlab.com/desamazonisons-internet/desamazonisons-internet'}
                       className="navbar-item">
                        Code source du site
                    </a>
                </div>
            </div>
        </nav>);
};

export default Header;
