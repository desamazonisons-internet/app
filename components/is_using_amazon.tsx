import React, {FunctionComponent, ReactNode, useState} from "react";
import fetch from 'isomorphic-unfetch';

interface Props {
}

interface Result {
    isProxied?: boolean;
    aws_region?: string;
    aws_service?: string;
}

const IsUsingAmazon: FunctionComponent<Props> = () => {
    const [requestPending, setRequestPending] = useState<boolean>(false);
    const [url, setUrl] = useState<string | null>(null);
    const [error, setError] = useState<string | null>(null);
    const [result, setResult] = useState<Result | null>(null);

    async function analyzeSite(): Promise<void> {
        setResult(null);
        setError(null);

        if (!url) {
            return;
        }

        try {
            setRequestPending(true);
            const response = await fetch(`/api/scan?url=${encodeURIComponent(url)}`);
            const json = await response.json();
            if (response.ok) {
                setResult(json);
            } else {
                setError(json['msg']);
            }
        } catch (e) {
            console.error(e);
            setError('Une erreur est survenue pendant le scan du site. Vous pouvez signer un bug sur https://gitlab.com/desamazonisons-internet/app/issues.');
        } finally {
            setRequestPending(false);
        }
    }

    function renderResult(): ReactNode {
        if (error) {
            return <div>{error}</div>
        }
        if (result === null) {
            return;
        }

        if (result.isProxied) {
            return (
                <div className="notification is-warning">
                    Il n'est pas possible de savoir si ce site est hébergé sur Amazon.
                    <div><small><em>Le site est hébergé derrière un Proxy du type Cloudflare. Pour en savoir plus,
                        vous pouvez consulter par exemple <a href={`https://censys.io/ipv4?q=${url}`}>le site de
                            Censys</a>.</em></small>
                    </div>
                </div>
            );
        } else if (!result.aws_region) {
            return (
                <div className="notification is-success">
                    Ce site ne semble pas être hébergé chez Amazon Web Services, félicitations !
                    <div><small><em>Attention la méthode de scan n'est pas exhaustive.</em></small></div>
                </div>
            );
        }
        return (
            <div className="notification is-danger">
                Ce site est hébergé chez Amazon Web Services, dans la région <code>{result.aws_region}</code>.
            </div>
        );
    }

    const buttonClasses = 'button is-info';

    return (<div>
            <div className="field has-addons">
                <div className="control is-expanded">
                    <input className="input is-info" type="text"
                           placeholder="Entrez une adresse valide. Exemple : www.amazon.fr"
                           value={url || ''}
                           onChange={event => {
                               setUrl(event.target.value);
                               setResult(null);
                           }}
                           onKeyDown={e => {
                               if (e.key === 'Enter') {
                                   analyzeSite();
                               }
                           }}
                           disabled={requestPending}
                           autoComplete="off"
                    />
                </div>
                <div className="control">
                    <button className={requestPending ? buttonClasses + ' is-loading' : buttonClasses}
                            onClick={_ => analyzeSite()} disabled={!url || requestPending}
                    >
                        Scanner
                    </button>
                </div>
            </div>
            <div style={{height: '100px'}}>
                {renderResult()}
            </div>
        </div>
    );
};

export default IsUsingAmazon;
